FROM openjdk:8
MAINTAINER Jacob Cabral <jacob-cabral@live.com>

ADD target/configuration-server.jar /usr/share/configuration-server/configuration-server.jar

EXPOSE 8888

CMD [ "java", "-jar", "/usr/share/configuration-server/configuration-server.jar" ]